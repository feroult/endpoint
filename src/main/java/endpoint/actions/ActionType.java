package endpoint.actions;

public enum ActionType {
	INDEX, SHOW, CREATE, UPDATE, CUSTOM
}
