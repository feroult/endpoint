package endpoint.actions;

import java.util.Map;

import endpoint.SimpleObject;
import endpoint.response.JsonResponse;
import endpoint.utils.JsonUtils;

public class SimpleObjectAction extends Action<SimpleObject> {

	@PUT("active")
	public JsonResponse activate(Long id) {
		SimpleObject object = r.query(SimpleObject.class).id(id);
		object.setAString("i was changed in action");
		r.save(object);
		return new JsonResponse(JsonUtils.to(object));
	}

	@PUT("params_action")
	public JsonResponse paramsAction(Long id, Map<String, String> params) {
		return new JsonResponse(params.get("x"));
	}

	@GET("me")
	public JsonResponse me() {
		return new JsonResponse("xpto");
	}

}
