package endpoint;

public class NotADatastoreObject {

	private String name;

	public NotADatastoreObject(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
